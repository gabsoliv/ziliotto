(function($){

	function initCarousels(){
		$('#carousel-banner').carousel();
		$('#carousel-testimony').carousel();
	}

	function init(){
		initCarousels();
	}

	$(window).ready(function(){
		init();
	});


	(function() {
		var nav = $('nav.navbar');
		var navTop = nav.offset().top;
	
		function doNavFix() {
			if($(this).scrollTop() > navTop) {
				nav.addClass('navbar-fixed');
			} else {
				nav.removeClass('navbar-fixed');
			}
		}

		$(window).resize(function() {
			nav.removeClass('navbar-fixed'); 
	
			navTop = nav.offset().top;
			doNavFix();
		});

		$(window).scroll(function() {
			doNavFix();
		})
	})();


	
})(jQuery);