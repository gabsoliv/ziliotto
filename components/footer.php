		<section class="social">
		    <div class="container">
		        <div class="row">
		            <div class="col-md-4">
						<a class="social__square" href="" target="_blank">
							<i class="icon-google"></i>
							<span>GOOGLE +</span>
						</a>

						<a class="social__square" href="" target="_blank">
							<i class="icon-twitter"></i>
							<span>TWITTER</span>
						</a>
					</div>

					<div class="col-md-4">
						<a class="social__square--full"  href="" target="_blank">
							<i class="icon-facebook"></i>
							<span>FACEBOOK</span>
						</a>
					</div>
					
		            <div class="col-md-4">
						<a class="social__square" href="" target="_blank">
							<i class="icon-youtube"></i>
							<span>YOUTUBE</span>
						</a>
						
						<a class="social__square" href="" target="_blank">
							<i class="icon-instagram"></i>
							<span>INSTAGRAM</span>
						</a>
					</div>
		        </div>
		    </div>
		</section>

		<footer class="footer text-center">
			<a class="footer__logo" href="" title="">
				<img class="center-block img-responsive" src="../assets/images/logo-footer.png" alt="" title="">
			</a>

			<div class="footer__copyright">
				<img class="footer__logo center-block img-responsive" src="../assets/images/copyright.png" alt="">
			</div>
		</footer>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script  src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="../assets/js/app.js"></script>
	</body>
	</html>