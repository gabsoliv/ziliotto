<html>
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Title</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../assets/stylesheets/css/style.css">
  </head>
<body>

<header class="header text-center">
    <div class="container">
        <a class="header__logo" href="index.php" title="Página Inicial">
            <img class="header__image img-responsive" src="../assets/images/logo.png" alt="Ziliotto Eventos" title="Ziliotto Eventos" />
        </a>
    </div>
</header>

  