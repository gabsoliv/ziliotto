<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="navbar__list text-center">
            <li class="navbar__item navbar__item--active">
                <a href="eventos-corporativos.php">Eventos Corporativos</a>
            </li>
            <li class="navbar__item">
                <a href="eventos-sociais.php">Eventos Sociais</a>
            </li>
            <li class="navbar__item">
                <a href="galeria.php">Galeria</a>
            </li>
            <li class="navbar__item">
                <a href="orcamento.php">Orçamento</a>
            </li>
            <li class="navbar__item">
                <a href="blog.php">Blog</a>
            </li>
            <li class="navbar__item">
                <a href="contato.php">Contato</a>
            </li>
    </ul>
    
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>