<?php include '../components/header.php';?>
<?php include '../components/navbar.php';?>

<section class="page-title">
    <div class="container">
        <div class="page-title__title text-center">
            <h1 class="title">Blog</h1>
        </div>
    </div>
</section>

<section class="breadcrumbs">
    <div class="container">
       <div class="col-xs-12">
        <ul class="text-center">
            <li><a href="" title="Página Inicial">Home</a></li>
            <li><a href="" title="blog">Blog</a></li>
            <li class="is-active">Lorem ipsum dolor sit...</li>
        </ul>
    </div>
</div>
</section>

<section class="blog-page">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-8">

             <div class="blog__featured">

                <article class="blog__post-featured">

                    
                    <figure class="blog__thumbnail">
                        <img class="img-responsive" src="https://dummyimage.com/750x400/000/fff" title="" alt="">
                    </figure>
                    
                    <h2 class="blog__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</h2>
                    
                    <div class="blog__excerpt">
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisi Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. 
                        </p>

                        <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                        </p>
                    </div>
                </article>
                    
                <div class="blog__post-info">
                    <p>
                        <i class="icon-tag"></i>
                        <a href="">Casamento,</a>
                        <a href="">Evento,</a>
                        <a href="">Felicidade</a>
                    </p>

                    <p>
                        <i class="icon-calendar"></i>
                        <a href="">21 Junho 2017</a>
                    </p>

                    <p>
                        <a href="">
                            <i class="icon-comments"></i>
                            3 Comentários
                        </a>
                    </p>
                </div>

                <div class="blog__post-related">
                    <div class="text-center">
                        <h3 class="title noupper blog-title">Post Relacionados</h3>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <article>
                                <figure>
                                    <img src="../assets/images/blog-img.jpg" alt="" />
                                </figure>

                                <h2>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</h2>
                            </article>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <article>
                                <figure>
                                    <img src="../assets/images/blog-img.jpg" alt="" />
                                </figure>

                                <h2>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</h2>
                            </article>
                        </div>
                    </div>
                </div>
            </div>

            <div class="blog__post-comments">
                    <div class="text-center">
                        <h3 class="title noupper blog-title">3 comentários</h3>
                    </div>
            </div>

        </div>

        <div class="col-xs-12 col-md-4">

            <aside class="blog__more-comments">
                <div class="blog-more-commented__title text-center">
                    <h3 class="title noupper">Post mais comentados</h3>
                </div>

                <article class="blog__post-featured">

                    <figure class="blog__thumbnail">
                        <img class="img-responsive" src="https://dummyimage.com/120x250/000/fff" title="" alt="">
                    </figure>

                    <h2 class="blog__title">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.
                        <a class="button--primary" href="" title="">Ler</a>
                    </h2>

                </article>
            </aside>

            <aside class="blog__post-category">
                <div class="blog-more-commented__title text-center">
                    <h3 class="title">CATEGORIAS</h3>
                </div>
                
                <ul>
                    <li>
                        <a href="">Lorem ipsum dolor sit amet, </a>
                    </li>
                    <li>
                        <a href="">Lorem ipsum dolor sit amet, </a>
                    </li>
                    <li>
                        <a href="">Lorem ipsum dolor sit amet, </a>
                    </li>
                </ul>
            </aside>

        </div>
    </div>
</div>
</section>


<?php include '../components/footer.php';?>