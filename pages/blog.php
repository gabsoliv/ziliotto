<?php include '../components/header.php';?>
<?php include '../components/navbar.php';?>

<section class="page-title">
    <div class="container">
        <div class="page-title__title text-center">
            <h1 class="title">Blog</h1>
        </div>
    </div>
</section>

<section class="breadcrumbs">
    <div class="container">
       <div class="col-xs-12">
        <ul class="text-center">
            <li><a href="" title="Página Inicial">Home</a></li>
            <li class="is-active">Blog</li>
        </ul>
    </div>
</div>
</section>

<section class="blog-page">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-md-8">

             <div class="blog__featured">

                <article class="blog__post-featured">

                    <h2 class="blog__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</h2>

                    <figure class="blog__thumbnail">
                        <img class="img-responsive" src="https://dummyimage.com/750x400/000/fff" title="" alt="">
                    </figure>

                    <div class="blog__excerpt">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisi...</p>
                    </div>

                    <div class="blog__read-more">
                        <a class="button--primary" href="blog-post.php" title="">Continue Lendo</a>
                    </div>

                </article>

                  <article class="blog__post-featured">

                    <h2 class="blog__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita eum commodi quaerat accusantium, corporis temporibus? Provident repellendus itaque vitae molestiae laborum culpa cumque sed nulla. Animi asperiores, iure. Repellat, nisi.</h2>

                    <figure class="blog__thumbnail">
                        <img class="img-responsive" src="https://dummyimage.com/750x400/000/fff" title="" alt="">
                    </figure>

                    <div class="blog__excerpt">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum quisquam reiciendis cum nobis, itaque eaque dolore excepturi tenetur beatae reprehenderit ea facilis alias corrupti atque inventore accusamus laudantium eos dolorum.</p>
                    </div>

                    <div class="blog__read-more">
                        <a class="button--primary" href="blog-post.php" title="">Continue Lendo</a>
                    </div>

                </article>

            </div>

            <div class="text-center">
                <ul class="pagination">
                    <li class="page-item active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">PRÓXIMA</a></li>
                    <li><a href="#">ANTERIOR</a></li>
                </ul>
            </div>


        </div>

        <div class="col-xs-12 col-md-4">

            <aside class="blog__more-comments">
                <div class="blog-more-commented__title text-center">
                    <h3 class="title noupper">Post mais comentados</h3>
                </div>

                <article class="blog__post-featured">

                    <figure class="blog__thumbnail">
                        <img class="img-responsive" src="https://dummyimage.com/120x250/000/fff" title="" alt="">
                    </figure>

                    <h2 class="blog__title">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.
                        <a class="button--primary" href="blog-post.php" title="">Ler</a>
                    </h2>

                </article>
            </aside>

            <aside class="blog__post-category">
                <div class="blog-more-commented__title text-center">
                    <h3 class="title">CATEGORIAS</h3>
                </div>
                
                <ul>
                    <li>
                        <a href="">Lorem ipsum dolor sit amet, </a>
                    </li>
                    <li>
                        <a href="">Lorem ipsum dolor sit amet, </a>
                    </li>
                    <li>
                        <a href="">Lorem ipsum dolor sit amet, </a>
                    </li>
                </ul>
            </aside>

        </div>
    </div>
</div>
</section>


<?php include '../components/footer.php';?>