<?php include '../components/header.php';?>
<?php include '../components/navbar.php';?>

<section class="page-title">
    <div class="container">
        <div class="page-title__title text-center">
            <h1 class="title">Contato</h1>
        </div>
    </div>
</section>

<section class="breadcrumbs">
    <div class="container">
         <div class="col-xs-12">
            <ul class="text-center">
                <li><a href="" title="Página Inicial">Home</a></li>
                <li class="is-active">Contato</li>
            </ul>
        </div>
    </div>
</section>

<section class="map-area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="map-area__content">
                    <img class="img-responsive" src="../assets/images/map-localization.png" title="" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">

                <div class="contact__title text-center">
                    <h2 class="title">Informações</h2>
                </div>
                <div class="contact__description">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro possimus fugiat, obcaecati eligendi autem.</p>
                </div>
                
                <ul class="contact__links">
                    <li>
                        <a href="tel:+5548996152418" title="Deixe uma mensagem!"><i class="icon-tel"></i> (48) 99615-2418</a>
                    </li>
                    <li>
                        <a href="mailto:ziliottoeventos@gmail.com" title="Mande um e-mail!"><i class="icon-email"></i> ziliottoeventos@gmail.com</a>
                    </li>
                </ul>
                
                <ul class="social__links">
                    <li><a href="" title=""><i class="icon-facebook"></i></a></li>
                    <li><a href="" title=""><i class="icon-instagram"></i></a></li>
                    <li><a href="" title=""><i class="icon-twitter"></i></a></li>
                    <li><a href="" title=""><i class="icon-google"></i></a></li>
                    <li><a href="" title=""><i class="icon-youtube"></i></a></li>
                </ul>

            </div>
            <div class="col-xs-12 col-md-8">
                <form class="contact__form" action="" method="POST">

                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <label for="nome" aria-labelledby="nome">
                                <input type="text" name="nome" placeholder="Nome">
                            </label>                    
                        </div>

                        <div class="col-xs-12 col-md-4">
                            <label for="email" class="required" aria-labelledby="email">
                                <input type="text" name="email" placeholder="E-mail">
                            </label>
                        </div>

                        <div class="col-xs-12 col-md-4">
                            <label for="telefone" aria-labelledby="telefone">
                                <input type="text" name="telefone" placeholder="Telefone">
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <label for="mensagem" class="required" aria-labelledby="mensagem">
                                <textarea name="mensagem" placeholder="Mensagem"></textarea>
                            </label>


                            <input type="submit" class="button--send" value="Enviar">
                        </div>   
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>


<?php include '../components/footer.php';?>