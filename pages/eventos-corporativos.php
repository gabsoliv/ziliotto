<?php include '../components/header.php';?>
<?php include '../components/navbar.php';?>

<section class="events">
    <div class="container">
        <div class="events__title text-center">
            <h1 class="title title--single">Eventos Corporativos</h1>
        </div>
    </div>
</section>

<section class="breadcrumbs">
    <div class="container">
        <ul class="text-center">
            <li><a href="" title="Página Inicial">Home</a></li>
            <li class="is-active">Eventos Corporativos</li>
        </ul>
    </div>
</section>

<section class="principal-events corporate-events">
    <div class="container">
        <div class="events-type">
         <div class="row">
             <div class="col-xs-12">
                <span class="events-type__label">Tipos :</span>
                <ul class="events-type__list">
                    <li class="events-type__itemlist events-type__itemlist--active"><a href="" title="">Congressos</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Simpósios</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Workshop</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Convenção</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Feiras</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Conferências</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Reuniões</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Inaugurações</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Festas Temáticas</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="event__description">
        <div class="row">

            <div class="col-xs-12">

                <figure class="event-description__image">
                    <img src="https://dummyimage.com/400x450/000/fff" title="" alt="">
                </figure>

                <h2 class="event-description__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>

                <div class="event-description__description">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae eum, tempora, mollitia eveniet amet cupiditate quidem fugiat aliquam nihil voluptate officia itaque minima excepturi iste, reprehenderit corrupti atque minus omnis.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae eum, tempora, mollitia eveniet amet cupiditate quidem fugiat aliquam nihil voluptate officia itaque minima excepturi iste, reprehenderit corrupti atque minus omnis.
                    </p>
                </div>
            </div>

        </div>
    </div>

    <div class="event__featured-types">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">

                <div class="event-featured-types__block block-1">
                    <h2 class="event-featured__title">Ações Promocionais e Live Marketing</h2>
                    <p class="event-featured__subtitle">Realizamos a Implantação de ações promocionais como abordagem, ativações, divulgações, com criterioso trabalho de seleção, execução e acompanhamento dos trabalhos da equipe.</p>
                </div>

            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">

                <div class="event-featured-types__block block-2">
                    <h2 class="event-featured__title">RH para Eventos</h2>
                    <p class="event-featured__subtitle"> Recebemos a demanda, verificamos a necessidade do cliente e em seguida começamos o processo de seleção que é feito minuciosamente.</p>
                </div>

            </div>
        </div>
    </div>


</div>
</section>

<section class="image-galery">
    <div class="container">
        <div class="events__title text-center">
            <h1 class="title noupper">Galeria</h1>
        </div>
        <!-- SLIDER -->
    </div>
</section>

<section class="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="contact-form__background">
                    <img src="../assets/images/bg-form.jpg" alt="" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="contact-form__description">
                    <span class="contact-form__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, placeat quisquam.</span>
                    <div class="contact-form__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis possimus, necessitatibus quis impedit suscipit ex, cum culpa nisi, eum provident velit distinctio magni quidem. Ab ex dolorem cupiditate mollitia maiores?</div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="contact-form__form">
                    <form action="" method="POST">
                        <label for="nome" aria-labelledby="nome">
                            <input type="text" name="nome" placeholder="Nome">
                        </label>

                        <label for="email" aria-labelledby="email">
                            <input type="text" name="email" placeholder="E-mail">
                        </label>

                        <label for="telefone" aria-labelledby="telefone">
                            <input type="text" name="telefone" placeholder="Telefone">
                        </label>

                        <label for="cidade" aria-labelledby="cidade">
                            <input type="text" name="cidade" placeholder="Cidade">
                        </label>

                        <label for="n_convidados" aria-labelledby="n_convidados">
                            <input type="text" name="n_convidados" placeholder="Número de convidados">
                        </label>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">

                                <label for="data_evento" aria-labelledby="data_evento">
                                    <input type="text" name="data_evento" placeholder="Data do evento">
                                </label>

                            </div>   
                            <div class="col-xs-12 col-sm-6">

                                <label for="data_evento" aria-labelledby="data_evento">
                                    <input type="text" name="data_evento" placeholder="Data do evento">
                                </label>

                            </div>   
                        </div>

                        <label for="tipo_evento" aria-labelledby="tipo_evento">
                            <input type="text" name="tipo_evento" placeholder="Tipo do Evento">
                        </label>

                        <label for="nome_empresa" aria-labelledby="nome_empresa">
                            <input type="text" name="nome_empresa" placeholder="Nome da empresa">
                        </label>

                        <input class="button--primary" type="submit" value="Enviar">

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include '../components/footer.php';?>