<?php include '../components/header.php';?>
<?php include '../components/navbar.php';?>

<section class="page-title">
    <div class="container">
        <div class="page-title__title text-center">
            <h1 class="title">Galeria</h1>
        </div>
    </div>
</section>

<section class="breadcrumbs">
    <div class="container">
        <ul class="text-center">
            <li><a href="" title="Página Inicial">Home</a></li>
            <li class="is-active">Galeria</li>
        </ul>
    </div>
</section>

<section class="principal-events corporate-events">
    <div class="container">
        <div class="events-type">
         <div class="row">
             <div class="col-xs-12">
                <span class="events-type__label">Categorias :</span>
                <ul class="events-type__list">
                    <li class="events-type__itemlist events-type__itemlist--active"><a href="" title="">Ver todas</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Casamentos</a></li>
                    <li class="events-type__itemlist"><a href="" title="">Corporativos</a></li>
                </ul>
            </div>
        </div>

        <div class="gallery__list">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="gallery__box">
                        <img class="img-responsive center-block" src="../assets/images/gallery-img.jpg" alt="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</p>
                        <a class="button--send" type="submit">RESPONDER</a>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="gallery__box">
                        <img class="img-responsive center-block" src="../assets/images/gallery-img.jpg" alt="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</p>
                        <a class="button--send" type="submit">RESPONDER</a>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="gallery__box">
                        <img class="img-responsive center-block" src="../assets/images/gallery-img.jpg" alt="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</p>
                        <a class="button--send" type="submit">RESPONDER</a>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="gallery__box">
                        <img class="img-responsive center-block" src="../assets/images/gallery-img.jpg" alt="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</p>
                        <a class="button--send" type="submit">RESPONDER</a>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="gallery__box">
                        <img class="img-responsive center-block" src="../assets/images/gallery-img.jpg" alt="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</p>
                        <a class="button--send" type="submit">RESPONDER</a> 
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="gallery__box">
                        <img class="img-responsive center-block" src="../assets/images/gallery-img.jpg" alt="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidun.</p>
                        <a class="button--send" type="submit">RESPONDER</a>
                    </div>
                </div>

                <div class="text-center">
                    <ul class="pagination">
                        <li class="page-item active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
</section>

<?php include '../components/footer.php';?>