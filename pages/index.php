<?php include '../components/header.php';?>

<section class="slider">
    <div id="carousel-banner" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="../assets/images/slider-01.jpg" alt="" />
            </div>

            <div class="item">
                <img src="../assets/images/slider-01.jpg" alt="" />
            </div>

            <div class="item">
                <img src="../assets/images/slider-01.jpg" alt="" />
            </div>
        </div>

    </div>
</section>

<?php include '../components/navbar.php';?>

<section class="events">
    <div class="container">
        <div class="events__title text-center">
            <h1 class="title">Eventos</h1>
        </div>

        <div class="events__text">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
        </div>

        <div class="events__banners">
            <div class="row">
                <div class="col-md-4 col-xs-4">
                    <div class="events__box">
                        <a href="eventos-corporativos.php" class="events__banners-item events__banners-item--corporativo">
                            <span>Eventos Corporativos</span>
                        </a>
                    </div>

                    <div class="events__box">
                        <a href="eventos-corporativos.php" class="events__banners-text events__banners-text--corporativo">
                            <i class="icon-arrow-top"></i>
                            <h2>Lorem ipsum</h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                        </a>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-4 col-xs-offset-4 col-xs-4">
                    <div class="events__box">
                        <a href="eventos-sociais.php" class="events__banners-text events__banners-text--corporativo">
                            <h2>Lorem ipsum</h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                            <i class="icon-arrow-bottom"></i>
                        </a>
                    </div>

                    <div class="events__box">
                        <a href="eventos-sociais.php" class="events__banners-item events__banners-item--corporativo">
                            <span>Eventos Sociais</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimony">
    <div class="container">
        <div class="testimony__title text-center">
            <h1 class="title title--white">Depoimentos</h1>
        </div>

        <div class="testimony__slider">
            <div class="row">
                <div id="carousel-testimony" class="carousel slide" data-ride="carousel">

                 <!-- Wrapper for slides -->
                 <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="col-md-10 center-block">
                            <div class="testimony__name">
                                <h2>Fulano &amp; Beltrana</h2>
                                <span>
                                    <i class="icon-heart-light"> </i>
                                    Casados em 21 de julho 2019
                                </span>
                            </div>
                            <div class="testimony__image">
                                <div class="mask-circle"> 
                                    <div class="mask-circle-img"> 
                                        <img src="../assets/images/testimony-image.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="testimony__text">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation...</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="col-md-10 center-block">
                            <div class="testimony__name">
                                <h2>Fulano &amp; Beltrana</h2>
                                <span>
                                    <i class="icon-heart-light"> </i>
                                    Casados em 21 de julho 2019
                                </span>
                            </div>
                            <div class="testimony__image">
                                <div class="mask-circle"> 
                                    <div class="mask-circle-img"> 
                                        <img src="../assets/images/testimony-image.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="testimony__text">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="image-galery">
    <div class="container">
        <div class="image-galery__title text-center">
            <h1 class="title">Galeria</h1>
        </div>
        <!-- SLIDER -->
    </div>
</section>

<section class="video-galery">
    <div class="container">
        <div class="video-galery__title text-center">
            <h1 class="title ">Videos</h1>
        </div>

    </div>
</section>

<section class="newsletter">
    <div class="container">
        <div class="newsletter__title text-center">
            <h1 class="title-alt">Newsletter</h1>
        </div>

        <form class="newsletter__form">
            <input type="text" name="email" placeholder="E-mail" />
            <i class="icon-email-newsletter"></i>
            <button>Enviar</button>
        </form>
    </div>
</section>

<section class="blog">
    <div class="container">
        <div class="events__title text-center">
            <h1 class="title">Blog</h1>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <article>
                    <a class="blog__wrapper" href="blog-post.php" title="" alt="">
                        <img class="img-responsive center-block" src="../assets/images/blog-01.jpg" alt="">
                        <h2 class="blog__title">Loren Ispum Dolor</h2>
                    </a>
                </article>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <article>
                    <a class="blog__wrapper" href="blog-post.php" title="" alt="">
                        <img class="img-responsive center-block" src="../assets/images/blog-01.jpg" alt="">
                        <h2 class="blog__title">Loren Ispum Dolor</h2>
                    </a>
                </article>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <article>
                    <a class="blog__wrapper" href="blog-post.php" title="" alt="">
                        <img class="img-responsive center-block" src="../assets/images/blog-01.jpg" alt="">
                        <h2 class="blog__title">Loren Ispum Dolor</h2>
                    </a>
                </article>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
                <a class="blog__link" href="">Todos os posts</a>
            </div>
        </div>
    </div>
</section>

<?php include '../components/footer.php';?>