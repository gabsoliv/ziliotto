<?php include '../components/header.php';?>
<?php include '../components/navbar.php';?>

<section class="page-title">
    <div class="container">
        <div class="page-title__title text-center">
            <h1 class="title">Orçamento</h1>
        </div>
    </div>
</section>

<section class="breadcrumbs">
    <div class="container">
         <div class="col-xs-12">
            <ul class="text-center">
                <li><a href="" title="Página Inicial">Home</a></li>
                <li class="is-active">Orçamento</li>
            </ul>
        </div>
    </div>
</section>

<section class="contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">

                <div class="contact__title text-center">
                    <h2 class="title">Informações</h2>
                </div>
                <div class="contact__description">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro possimus fugiat, obcaecati eligendi autem.</p>
                </div>
                
            </div>
            <div class="col-xs-12 col-md-8">
                <form class="contact__form mt-150" action="" method="POST">

                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <label for="nome" aria-labelledby="nome">
                                <input type="text" name="nome" placeholder="Nome">
                            </label>                    
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <label for="telefone" aria-labelledby="telefone">
                                <input type="text" name="telefone" placeholder="Telefone">
                            </label>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <label for="email" aria-labelledby="email">
                                <input type="text" name="email" placeholder="E-mail">
                            </label>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <label for="telefone" aria-labelledby="telefone">
                                <input type="text" name="cidade" placeholder="Cidade">
                            </label>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <label for="telefone" aria-labelledby="telefone">
                                <input type="text" name="date" placeholder="Para quando é o evento?">
                            </label>
                        </div>
                    </div>

                    <div class="contact__select">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p>Evento:</p>  
                                <label><input type="radio" name="corporativo">Corporativo</label>
                                <label><input type="radio" name="Social">Social</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <label for="tipo-evento" aria-labelledby="tipo-evento">
                                <input type="text" name="tipo-evento" placeholder="Tipo de evento">
                            </label>
                        </div>  

                        <div class="col-xs-12 col-md-6">
                            <label for="nome-empresa" aria-labelledby="nome-empresa">
                                <input type="text" name="tipo-evento" placeholder="Nome da empresa">
                            </label>
                        </div> 
                        
                        <div class="col-xs-12 col-md-6">
                            <input type="submit" class="button--send" value="Enviar">
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
</section>


<?php include '../components/footer.php';?>